<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Texto com Efeito de Mais Conteúdo</title>
<style>
    .text-preview {
        position: relative;
        max-height: 50px; /* Ajuste conforme necessário */
        overflow: hidden;
        padding-right: 120px; /* Espaço para o botão 'Leia mais' */
    }

    .text-preview::after {
        content: '';
        position: absolute;
        right: 0;
        top: 0;
        width: 100px; /* Largura do efeito de desvanecimento */
        height: 100%;
        background: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1));
    }

    .read-more {
        position: absolute;
        right: 10px;
        top: 50%; /* Centraliza verticalmente */
        transform: translateY(-50%);
        background-color: white; /* Cor de fundo do botão */
        padding: 5px;
        cursor: pointer;
    }
</style>
</head>
<body>
<div class="text-preview">
    Este é um parágrafo de exemplo que mostra como você pode esconder parte do texto para incentivar os usuários a clicarem em "Leia mais" para ver o conteúdo completo. Este texto continua e tem mais informações além do que você pode ver inicialmente.
    <span class="read-more">Leia mais</span>
</div>

<script>
    document.querySelector('.read-more').addEventListener('click', function() {
    var container = document.querySelector('.text-preview');
    container.style.maxHeight = 'none';  // Remove a altura máxima
    container.style.overflow = 'visible';  // Mostra todo o conteúdo
    this.style.display = 'none';  // Esconde o botão 'Leia mais'
});
</script>
</body>
</html>
