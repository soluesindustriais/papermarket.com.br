<?php 
$h1 = "Friedrich Nietzche";
$title  =  "Friedrich Nietzche?";
$desc = "Compare Friedrich Nietzche?, você vai encontrar na plataforma Soluções Industriais, receba uma estimativa de valor pela internet com aproximadamen";
$key  = "Carga Indivisível Categoria D, Comprar Carga Indivisível Categoria D";
include "inc/transporte-de-cargas-invidisiveis/transporte-de-cargas-invidisiveis-linkagem-interna.php";
include "inc/head.php"; 
?>

</head>

<body> <?php include "inc/topo.php"; ?>
    <main>
        <div class="content">
            <section> <?php "$caminhoartigos" ?>
                <div class="wrapper-produtos">

                    <article>
                        <div class="article-content">
                            <h2>Conheça Friedrich Nietzsche</h2>
                            <h3>Quem foi Nietzsche?</h3>
                            <p>Friedrich Nietzsche foi um filósofo alemão do século XIX, conhecido por suas ideias inovadoras e provocativas. Nasceu em 1844 e faleceu em 1900.</p>
                            <h3>Principais Conceitos</h3>
                            <p>Nietzsche é conhecido por conceitos como "Além-do-Homem" (Übermensch), "Vontade de Poder" e "Eterno Retorno". Ele questionou a moralidade, a religião e a filosofia tradicional, desafiando ideias estabelecidas.</p>
                            <h3>Obras Importantes</h3>
                            <p>Entre suas obras mais conhecidas estão "Assim Falou Zaratustra", "Além do Bem e do Mal" e "Genealogia da Moral". Nestes textos, Nietzsche explorou a natureza humana e a condição existencial.</p>
                            <h3>Influência e Legado</h3>
                            <p>Nietzsche influenciou profundamente a filosofia, a psicologia, a literatura e diversas outras áreas. Sua abordagem única e visão de mundo continua a inspirar pensadores até os dias atuais.</p>
                            <p>Para saber mais, consulte fontes especializadas em filosofia e literatura.</p>
                        </div>

                    </article>
                    <div class="coluna-artigo">
                        <?php include "inc/artigos/artigos-coluna-lateral.php" ?>
                        <button class="botao-cotar botao-artigo" rel="nofollow" title="Transporte De Cargas"><i class="fa fa-envelope"></i>Solicite um orçamento</button>
                        <div class="botao-mobile">
                            <button class="botao-cotar botao-artigo-mobile" rel="nofollow" title="Transporte De Cargas"><i class="fa fa-envelope"></i>Solicite um orçamento</button>
                        </div>
                    </div>
                    <br class="clear"><?php include "inc/regioes-brasil.php"; ?>
            </section>
        </div>
    </main>
    </div><!-- .wrapper --> <?php include "inc/footer.php"; ?>
    <!-- Tabs Regiões -->
    
    <script defer src="<?php echo $url ?>js/organictabs.jquery.js"> </script>

</body>

</html>