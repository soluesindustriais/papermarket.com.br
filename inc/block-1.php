<div class="container apresentation">
	<div class="wrapper">
		<section>
			<div>
				<div class="grid-col-2-3 py-5 about">
					<div class="d-flex flex-column">
						<h1 class="about__title">
							<?= $nomeSite ?>
						</h1>
					</div>
					<div>
						<p>Um produto considerado 100% seguro, isento da possibilidade de propagação de vírus e bactérias, afinal você só encostará apenas nas folhas que irá utilizar, não tendo contato com as demais, principalmente quando as toalhas de papel estão dentro de dispensers</p>
						<p><a href="<?$url?>produtos-categoria" title="" class="btn">Mais informações</a></p>
					</div>
				</div>
			</div>
		</section>
	</div>
	<hr>
</div>
<section>
	<div>
		<div class="container">
			<div class="wrapper">
				<div class="clientes">
					<h2 class="title-underline clientes__title fs-28 text-center">Nossos produtos</h2>
					<div class="produtos__carousel">
						<?php
						foreach ($vetCategorias as $categoria) {
							$categoriaSemAcento = remove_acentos($categoria);
							$categoriaComEspacos = str_replace('-', ' ', $categoria);
							echo "<div class=\"item-slide\">
						            <a href=\"{$url}{$categoriaSemAcento}-categoria\" title=\"categoria {$categoriaComEspacos} de {$nomeSite}\" class=\"card card--overlay\">
                						<img class=\"card__image\" src=\"{$url}imagens/{$categoriaSemAcento}/{$categoriaSemAcento}-01.webp\" alt=\"Categoria {$categoriaComEspacos} de {$nomeSite}\" title=\"categoria {$categoriaComEspacos} de {$nomeSite}\">
                						<h3 class=\"card__title\">{$categoriaComEspacos}</h3>
                						<span class=\"card__action\">Saiba mais</span>
						            </a>
          						  </div>";
						}
						?>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>