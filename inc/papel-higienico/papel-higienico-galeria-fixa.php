
 <link rel="stylesheet" href="<?php echo $url ?>css/thumbnails.css">
 <div class="galeria-fixa">
     <ul class="thumbnails-mod17">
         <?php
 
         shuffle($VetPalavrasPapel_higienico ); // Embaralha o array de palavras
 
         for ($i = 1; $i <= 4; $i++) {
             $categoriaNameSemAcento = strtolower(remove_acentos($CategoriaNamePapel_higienico)); // Remover acentos e substituir espaços por hifens
             $palavraAtual = $VetPalavrasPapel_higienico [$i - 1]; // Obtém a palavra atual (ajustando o índice para começar de 0)
             $palavraSemAcento = strtolower(str_replace(" ", "-", remove_acentos($palavraAtual)));
 
             echo "<li><a class=\"lightbox\" href=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" title=\"" . $palavraAtual . "\"><img src=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" alt=\"" . $palavraAtual . "\" title=\"" . $palavraAtual . "\"></a></li>";
         }
         ?>
     </ul>
 </div>