
        <aside class="mpi-aside">
            <?php if (!$isMobile) : ?>
                <button class="aside__btn botao-cotar" title="<?php echo $h1 ?>">Solicite um orçamento</button>
            <?php else : ?>
                <button class="botao-cotar botao-cotar-mobile">Solicitar Orçamento</button>
            <?php endif; ?>
            <div class="aside__menu">
                <h2><a href="<?php echo $url ?>papel-higienico-categoria" title="Produtos relacionados <?php echo $nomeSite ?>">Papel Higiênico<br>Produtos relacionados</a></h2>
                <nav>
                    <ul> <?php include('inc/papel-higienico/papel-higienico-sub-menu.php'); ?> </ul>
                </nav> <br>
            </div>
        <div class="aside__menu">
                <h2><a href="<?php echo $url ?>produtos-categoria" title="Outras Categorias">Outras Categorias </a></h2>
                <nav class="no-scroll">
                    <ul> <?php include('inc/papel-higienico/papel-higienico-sub-menu-categoria.php'); ?> </ul>
                </nav> <br>
            </div>
        </aside>