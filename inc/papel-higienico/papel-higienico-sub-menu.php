<?php

        foreach ($VetPalavrasPapel_higienico as $palavra) {
            $palavraSemAcento = strtolower(remove_acentos($palavra));
            $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $palavra));
        
            echo "<li><a class=\"link-sub-list first-child\" href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>\n";
        }
        
        ?>