
        
<?php
include 'papel-higienico-api-produtos.php';
?>

<!-- produtos premium -->
<ul class="thumbnails">
    <?php
    $count = 0; // contador para mostrar os produtos

    foreach ($products as $item) {
        if ($count >= 4) {
            break;
        }

        // caminho para pegar os componentes da API
        $product_name = $item['_source']['title'] ?? 'Soluções Industriais';
        $caminho_image = $item['_source']['cover'] ?? 'imagem_padrao.jpg';
        $cliente = $item['_source']['company']['name'] ?? 'Soluções Industriais';

        $img = $caminho_image;
        $rand_num = rand(1, 4);

        // verifica e altera a extensão da imagem para WEBP
        if (imageExist($img)) {
            if (strpos($caminho_image, '.jpg') !== false) {
                $caminho_image_webp = str_replace('.jpg', '.webp', $caminho_image);
            } elseif (strpos($caminho_image, '.png') !== false) {
                $caminho_image_webp = str_replace('.png', '.webp', $caminho_image);
            } else {
                $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
            }
        } else {
            $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
        }

        echo '<li class="overflow-hidden nova-api">
            <img src="' . $caminho_image_webp . '" alt="' . $product_name . '" title="' . $product_name . '">
            <div class="padding-premium">
                <p><b>' . $cliente . '</b></p>
                <h2>' . $product_name . '</h2>
                <button class="botao-cotar" rel="nofollow" title="' . $product_name . '">COTAR AGORA</button>
            </div>
          </li>';

        $count++;
    }
    ?>
</ul>

<!-- PRODUTOS FIXOS -->
<?php
for ($i = 2; $i < 4; $i++) {
    if (isset($products[$i])) {
        $item = $products[$i];
        $product_name = $item['_source']['title'] ?? 'Soluções Industriais';
        $caminho_image = $item['_source']['cover'] ?? 'imagem_padrao.jpg';
        $descricao = $item['_source']['description'] ?? 'Descrição não disponível';
        $cliente = $item['_source']['company']['name'] ?? 'Soluções Industriais';

        // variável para tratar as imagens da API
        $img = $caminho_image;

        // verifica e altera a extensão da imagem para WEBP
        if (imageExist($img)) {
            if (strpos($caminho_image, '.jpg') !== false) {
                $caminho_image_webp = str_replace('.jpg', '.webp', $caminho_image);
            } elseif (strpos($caminho_image, '.png') !== false) {
                $caminho_image_webp = str_replace('.png', '.webp', $caminho_image);
            } else {
                $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
            }
        } else {
            $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
        }

        echo '<div class="box-produto nova-api">
                <div class="grid">
                    <div class="col-3 box-img-random overflow-hidden">
                        <a class="lightbox" href="' . $caminho_image_webp . '" title="' . $product_name . '" target="_blank">
                            <img src="' . $caminho_image_webp . '" title="' . $product_name . '" alt="' . $product_name . '">
                        </a>
                    </div>
                    <div class="col-9">
                        <h2>' . $product_name . '</h2>
                        <h3><b>' . $cliente . '</b></h3>
                        <p>' . $descricao . '</p>
                        <button class="botao-cotar" rel="nofollow" title="' . $product_name . '">COTAR AGORA</button>
                    </div>
                </div>
              </div>';
    }
}
?>
<!-- FIM PRODUTOS FIXOS -->

<!-- chamada das imagens fixas -->
<?php include 'inc/papel-higienico/papel-higienico-imagens-fixos.php'; ?>

<!-- PRODUTOS RANDOM -->
<?php
for ($i = 7; $i < 14; $i++) {
    if (isset($products[$i])) {
        $item = $products[$i];
        $product_name = $item['_source']['title'] ?? 'Soluções Industriais';
        $caminho_image = $item['_source']['cover'] ?? 'imagem_padrao.jpg';
        $descricao = $item['_source']['description'] ?? 'Descrição não disponível';
        $cliente = $item['_source']['company']['name'] ?? 'Soluções Industriais';

        // variável para tratar as imagens da API
        $img = $caminho_image;

        // verifica e altera a extensão da imagem para WEBP
        if (imageExist($img)) {
            if (strpos($caminho_image, '.jpg') !== false) {
                $caminho_image_webp = str_replace('.jpg', '.webp', $caminho_image);
            } elseif (strpos($caminho_image, '.png') !== false) {
                $caminho_image_webp = str_replace('.png', '.webp', $caminho_image);
            } else {
                $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
            }
        } else {
            $caminho_image_webp = 'imagens/papel-higienico/papel-higienico-' . $rand_num . '.webp';
        }

        echo '<div class="box-produto nova-api">
                <div class="grid">
                    <div class="col-3 box-img-random overflow-hidden">
                        <a class="lightbox" href="' . $caminho_image_webp . '" title="' . $product_name . '" target="_blank">
                            <img src="' . $caminho_image_webp . '" title="' . $product_name . '" alt="' . $product_name . '">
                        </a>
                    </div>
                    <div class="col-9">
                        <h2>' . $product_name . '</h2>
                        <h3><b>' . $cliente . '</b></h3>
                        <p>' . $descricao . '</p>
                        <button class="botao-cotar" rel="nofollow" title="' . $product_name . '">COTAR AGORA</button>
                    </div>
                </div>
              </div>';
    }
}
?>
    
    