<?php

function criarpagina($nome_do_arquivo, $nome_da_categoria)
{

    for ($i = 0; $i < count($nome_do_arquivo); $i++) {

        if (!file_exists($nome_do_arquivo[$i])) {
            $h1dapagina = ucfirst(trim($nome_do_arquivo[$i]));
            $categoriapagina = remove_acentos(str_replace(' ', '-', strtolower(trim($nome_da_categoria[$i]))));
            $nomedoarquivo = remove_acentos(str_replace(' ', '-', strtolower($h1dapagina)));
            $conteudo = '
<?php
$h1 = "' . $h1dapagina . '";
$title  =  "' . $h1dapagina . '";
$desc = "Compare ' . $h1dapagina . ' você vai encontrar na plataforma Soluções Industriais, receba uma estimativa de valor pela internet com aproximadamen";
$key  = "' . $h1dapagina . ', Comprar ' . $h1dapagina . '";
include "inc/head.php"; 
include "inc/' . $categoriapagina . '/' . $categoriapagina . '-vetPalavras.php";
?>
</head>
<body> 
<?php include "inc/topo.php"; ?> 
        <main>
            <div class="content">
                <section>  
                <?php include "inc/auto-breadcrumb.php" ?>
                <div class="wrapper-produtos">
                <?php include "inc/' . $categoriapagina . '/' . $categoriapagina . '-buscas-relacionadas.php"; ?> 
                <br class="clear">
                    <article>
                        <div class="article-content">
                            <div class="ReadMore">
                                <p>Realize uma cotação de ' . $h1dapagina . ', você só vai descobrir no site do Soluções Industriais, realize um orçamento imediatamente com dezenas de empresas de todo o Brasil gratuitamente para todo o Brasil</p>
                                <p>Possuindo centenas de empresas, o Soluções Industriais é a ferramenta business to business mais completo da área industrial. Para realizar um orçamento de <?php echo $h1 ?>, clique em um ou mais dos anuciantes a seguir:</p>
                                <span></span>
                            </div>
                        </div>
                        <hr> 
                        <?php
                            include "inc/' . $categoriapagina . '/' . $categoriapagina . '-produtos-premium.php";
                            include "inc/produtos-fixos.php";
                            include "inc/' . $categoriapagina . '/' . $categoriapagina . '-imagens-fixos.php";
                            include "inc/produtos-random.php";
                        ?>                            
                        <hr>
                        <h2>Galeria de Imagens Ilustrativas</h2> 
                        <?php include "inc/' . $categoriapagina . '/' . $categoriapagina . '-galeria-fixa.php" ?> 
                        <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                    </article> 
                    <?php include "inc/' . $categoriapagina . '/' . $categoriapagina . '-coluna-lateral.php" ?>
                    <br class="clear">
                    <?php include "inc/regioes-brasil.php" ?>
                    </div>
                </section>
            </div>
        </main>
        <?php include "inc/footer.php" ?>
        <!-- Tabs Regiões -->
        <script defer src="<?php echp $url ?>js/organictabs.jquery.js"> </script>
</body>
</html>
';
            file_put_contents($nomedoarquivo . ".php", $conteudo);
            criarmenujson($nome_da_categoria[$i], $categoriapagina, $h1dapagina);
            criararquivotemporario($nome_da_categoria[$i], $nome_do_arquivo[$i]);
        }
    }
}
$estudodepalavras = file_get_contents('inc/classes/estudo/palavras.php');
$estudodepalavras = trim($estudodepalavras);
$estudodepalavras = explode(",", $estudodepalavras);


$estudodecategorias = file_get_contents('inc/classes/estudo/categoria.php');
$estudodecategorias = trim($estudodecategorias);
$estudodecategorias = explode(",", $estudodecategorias);

if ($estudodepalavras[0] != "") {
    $arquivosparatemp = ["palavras.php", "categoria.php"];
    if (count($estudodepalavras) == count($estudodecategorias)) {
        criarpagina($estudodepalavras, $estudodecategorias);
        newcategoria($estudodecategorias);
        apagarConteudoArquivo("inc/classes/estudo/palavras.php");
        apagarConteudoArquivo("inc/classes/estudo/categoria.php");
    }
}

function apagarConteudoArquivo($caminhoArquivo)
{
    $arquivo = fopen($caminhoArquivo, 'w');
    if ($arquivo) {
        fclose($arquivo);
        return true;
    } else {
        return false;
    }
}

function criarmenujson($categorianome, $categoriatratata, $palavra)
{

    $jsonString = file_get_contents('js/menu-items.json');
    $data = json_decode($jsonString, true);

    if (json_last_error() != JSON_ERROR_NONE) {
        echo "Erro ao decodificar JSON: " . json_last_error_msg();
        exit;
    }

    $palavratratada = remove_acentos(str_replace(' ', '-', strtolower(trim($palavra))));

    $categorianome = trim($categorianome);

    if (!isset($data['Produtos']['submenu'][$categorianome])) {
        $data['Produtos']['submenu'][$categorianome] = [
            'url' => "$categoriatratata-categoria",
            'submenu' => []
        ];
    }

    if (!isset($data['Produtos']['submenu'][$categorianome]['submenu'])) {
        $data['Produtos']['submenu'][$categorianome]['submenu'] = [];
    }

    $data['Produtos']['submenu'][$categorianome]['submenu'][$palavra] = [
        'url' => $palavratratada
    ];

    $jsonModified = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    if (json_last_error() != JSON_ERROR_NONE) {
        echo "Erro ao codificar JSON: " . json_last_error_msg();
        exit;
    }

    file_put_contents('js/menu-items.json', $jsonModified);
}

function newcategoria($estudodecategorias)
{
    $filename = 'inc/vetCategorias.php';
    include 'inc/vetCategorias.php';

    function newcateg($filename, $conteudo)
    {
        if (!file_exists($filename)) {
            die("O arquivo $filename não existe.");
        }
        $fileContents = file_get_contents($filename);
        if ($fileContents === false) {
            die("Não foi possível ler o arquivo.");
        }
        $position = strpos($fileContents, '[');
        if ($position === false) {
            die("O caractere '[' não foi encontrado no arquivo.");
        }
        $newconteudo = '"' . $conteudo . '",';
        $newContents = substr($fileContents, 0, $position + 1) . $newconteudo . substr($fileContents, $position + 1);
        if (file_put_contents($filename, $newContents) === false) {
            die("Não foi possível escrever no arquivo.");
        }
    }

    $uniqueEstudoCategorias = array_unique($estudodecategorias);

    foreach ($uniqueEstudoCategorias as $categoriaNova) {
        if (!in_array($categoriaNova, $vetCategorias)) {
            newcateg($filename, str_replace(' ', '-', strtolower(trim($categoriaNova))));
        }
    }
}
function criararquivotemporario($categoria, $palavra)
{
    $pathtemp = "inc/classes/temp/temp.php";
    $palavra  = str_replace(" ", "-", strtolower(trim($palavra)));
    $categoria =  str_replace(" ", "_", strtolower(trim($categoria)));
    startemporario($pathtemp);
    if (file_exists($pathtemp)) {
        include "$pathtemp";
    }

    if (isset($$categoria)) {
        $arquivotemp = file_get_contents($pathtemp);
        $textoAntigo = "$" . $categoria . " = [";
        $textoNovo = "$" . $categoria . " = ['" . $palavra . "',";
        $conteudoModificado = str_replace($textoAntigo, $textoNovo, $arquivotemp);

        file_put_contents($pathtemp, $conteudoModificado);
    } else {
        $newconteudo = "\n$" . $categoria . " = ['" . $palavra . "'];";
        file_put_contents($pathtemp, $newconteudo, FILE_APPEND);
    }
}

function startemporario($pathtemp)
{
    $conteudoAtual = file_get_contents($pathtemp);
    if (strpos($conteudoAtual, "<?php") === false) {
        $conteudo = "<?php\n";
        file_put_contents($pathtemp, $conteudo, FILE_APPEND);
    }
}
function addVetPalavras()
{
    include "inc/vetCategorias.php";
    foreach ($vetCategorias as $categoria) {
            if (file_exists("inc/$categoria/$categoria-vetPalavras.php")) {
            $categoria = remove_acentos(str_replace(' ', '-', strtolower(trim($categoria))));
            $filename = "inc/$categoria/$categoria-vetPalavras.php";
            include "$filename";
            $categoria = remove_acentos(str_replace('-', '_', strtolower(trim($categoria))));

            $categoriaMaiusculo = str_replace('-',"_",ucfirst($categoria));


            $nomedavet = "VetPalavras" . $categoriaMaiusculo;

            $find  = "$nomedavet = [";

            include "inc/classes/temp/temp.php";

            


            foreach ($$categoria as $categname) {
                if (!in_array($categname, $$nomedavet)) {
                    $content = file_get_contents($filename);
                    $content = str_replace($find, $find . '"' . $categname . '",', $content);
                    file_put_contents($filename, $content);
                }
            }
        }
        else {
            exit;
        }
} 
}

if (file_exists("inc/classes/temp/temp.php")) {
    addVetPalavras();
}
