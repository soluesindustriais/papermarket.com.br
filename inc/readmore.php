<script src="<?= $url ?>js/readmore.min.js"></script>
<script>
    initReadMore('.ReadMore', {
        collapsedHeight: 220,
        heightMargin: 30,
    });
</script>