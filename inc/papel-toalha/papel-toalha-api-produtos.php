
    <?php
$curl = curl_init();
$seed = rand(1, 10000);

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://169.57.169.94:9200/advertiser3/_search',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 15,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_POSTFIELDS => '{
	  "size": 25,
	  "query": {
	    "function_score": {
	  	"query": {
	  	  "bool": {
	  		"must": [
	  		  {
	  			"multi_match": {
	  			  "query": "papel toalha",
	  			  "fields": ["title"],
	  			  "type": "best_fields"
	  			}
	  		  },
	  		  {
	  			"term": {
	  			  "status.keyword": "active"
	  			}
	  		  }
	  		]
	  	  }
	  	},
	  	"random_score": {
	  	  "seed": ' . $seed . '
	  	}
	    }
	  }
}',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Basic ZWxhc3RpYzpQUGpCakdOQm94SnJDV2Eyd2lRbg=='
    ),
));

$response = curl_exec($curl);
curl_close($curl);

// Converte a resposta JSON para um array associativo
$data = json_decode($response, true);

// Verifica se há resultados
if (isset($data['hits']['hits']) && !empty($data['hits']['hits'])) {
    $products = $data['hits']['hits'];
} else {
    $products = [];
}



// verifica se a imagem existe
function imageExist($url)
{
    $headers = @get_headers($url);
    return (is_array($headers) && strpos($headers[0], '200') !== false);
}
?>
    