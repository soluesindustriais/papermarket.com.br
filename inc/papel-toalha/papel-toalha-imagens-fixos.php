
        <div style="display: flex;">
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?php echo $url ?>imagens/papel-toalha/papel-toalha-01.webp" class="lightbox" title="<?php echo $h1 ?>" target="_blank">
                        <img src="<?php echo $url ?>imagens/papel-toalha/papel-toalha-01.webp" alt="<?php echo $h1 ?>" title="<?php echo $h1 ?>">
                    </a>
                    <strong>Imagem ilustrativa de <?php echo $h1 ?></strong>
                </div>
            </div>
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?php echo $url ?>imagens/papel-toalha/papel-toalha-02.webp" class="lightbox" title="<?php echo $h1 ?>" target="_blank">
                        <img src="<?php echo $url ?>imagens/papel-toalha/papel-toalha-02.webp" alt="<?php echo $h1 ?>" title="<?php echo $h1 ?>">
                    </a>
                    <strong>Imagem ilustrativa de <?php echo $h1 ?></strong>
                </div>
            </div>
        </div>