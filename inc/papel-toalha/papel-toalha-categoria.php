<?php

  $categoriaNameSemAcento = strtolower(remove_acentos($CategoriaNamePapel_toalha)); // Remover acentos e substituir espaços por hifens

  foreach ($VetPalavrasPapel_toalha  as $palavra) {
      $palavraSemAcento = strtolower(remove_acentos($palavra)); // Remover acentos e substituir espaços por hifens
      $palavraUpperCaseSemHifen = ucwords(str_replace("-", " ", $palavra)); // Substituir hifens por espaços e colocar em maiúsculas

      // Gerar número aleatório entre 1 e 4
      $numeroAleatorio = rand(1, 4);

      // Construir a estrutura LI
      echo "<li>
                <a rel=\"nofollow\" href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraUpperCaseSemHifen\">
                    <div class=\"overflow-hidden\"> 
                        <img src=\"imagens/$categoriaNameSemAcento/$categoriaNameSemAcento-$numeroAleatorio.webp\" alt=\"$palavraUpperCaseSemHifen\" title=\"$palavraUpperCaseSemHifen\">
                    </div>
                    <div class=\"title-produtos\">
                        <h2>$palavraUpperCaseSemHifen</h2>
                    </div>
                </a> 
            </li>";}

  ?>