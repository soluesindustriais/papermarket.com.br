<div class="container">
    <div class="wrapper">
        <h2 class="title-underline fs-28 text-center">Produtos em destaque</h2>
        <ul class="thumbnails-main grid-col-4">
            <?php
            $vetPalavras = [
                "comprar-guardanapo",
                "papel-higienico-ecologico",
                "papel-higienico-rolao",
                "papel-toalha-interfolha"

            ];

            foreach ($vetPalavras as $itemDestaque) {
                $itemDestaqueSemAcento = remove_acentos($itemDestaque);
                $itemDestaqueComEspacos = str_replace('-', ' ', $itemDestaque);

                echo "<li>
						<a href=\"{$url}{$itemDestaqueSemAcento}\" title=\"{$itemDestaqueComEspacos}\">
							<div class=\"overflow-hidden\">
								<img loading=\"lazy\" src=\"{$url}imagens/thumbs/{$itemDestaqueSemAcento}.webp\" alt=\"{$itemDestaqueComEspacos}\">
							</div>
							<div class=\"title-produtos\">
								<h2>{$itemDestaqueComEspacos}</h2>
							</div>
						</a>
					</li>";
            }
            ?>

        </ul>
    </div>

</div>