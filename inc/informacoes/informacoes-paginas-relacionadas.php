
             <div class="busca-relacionadas-mpi">
                 <b>Páginas relacionadas</b>
                 <ul>
                     <?php
                     $random = array();
                     $limit = 3;
             
                     foreach ($VetPalavrasInformacoes  as $pagina) {
                         $palavraSemAcento = strtolower(remove_acentos($pagina));
                         $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $pagina));
             
                         $random[] = "<li><a href=\"" . $url . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>";
                     }
             
                     shuffle($random);
                     for ($i = 0; $i < min($limit, count($random)); $i++) {
                         echo $random[$i];
                     }
                     ?>
                 </ul>
             </div>