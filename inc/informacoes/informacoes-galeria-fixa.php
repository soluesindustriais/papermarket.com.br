
 <link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
 <div class="galeria-fixa">
     <ul class="thumbnails-mod17">
         <?php
 
         shuffle($VetPalavrasInformacoes ); // Embaralha o array de palavras
 
         for ($i = 1; $i <= 4; $i++) {
             $categoriaNameSemAcento = strtolower(remove_acentos($CategoriaNameInformacoes)); // Remover acentos e substituir espaços por hifens
             $palavraAtual = $VetPalavrasInformacoes [$i - 1]; // Obtém a palavra atual (ajustando o índice para começar de 0)
             $palavraSemAcento = strtolower(str_replace(" ", "-", remove_acentos($palavraAtual)));
 
             echo "<li><a class=\"lightbox\" href=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" title=\"" . $palavraAtual . "\"><img src=\"" . $url . "imagens/" . $categoriaNameSemAcento . "/" . $categoriaNameSemAcento . "-$i.webp\" alt=\"" . $palavraAtual . "\" title=\"" . $palavraAtual . "\"></a></li>";
         }
         ?>
     </ul>
 </div>