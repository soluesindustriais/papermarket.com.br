
        <div style="display: flex;">
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/informacoes/informacoes-01.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img src="<?= $url ?>imagens/informacoes/informacoes-01.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
            <div class="col-6">
                <div class="picture-legend picture-center">
                    <a href="<?= $url ?>imagens/informacoes/informacoes-02.webp" class="lightbox" title="<?= $h1 ?>" target="_blank">
                        <img src="<?= $url ?>imagens/informacoes/informacoes-02.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                    </a>
                    <strong>Imagem ilustrativa de <?= $h1 ?></strong>
                </div>
            </div>
        </div>