
        <aside class="mpi-aside">
            <?php if (!$isMobile) : ?>
                <button class="aside__btn botao-cotar" title="<?= $h1 ?>">Solicite um orçamento</button>
            <?php else : ?>
                <button class="botao-cotar botao-cotar-mobile">Solicitar Orçamento</button>
            <?php endif; ?>
            <div class="aside__menu">
                <h2><a href="<?= $url ?>informacoes-categoria" title="Produtos relacionados <?= $nomeSite ?>">Informações<br>Produtos relacionados</a></h2>
                <nav>
                    <ul> <? include('inc/informacoes/informacoes-sub-menu.php'); ?> </ul>
                </nav> <br>
            </div>
        <div class="aside__menu">
                <h2><a href="<?= $url ?>produtos-categoria" title="Outras Categorias">Outras Categorias </a></h2>
                <nav class="no-scroll">
                    <ul> <? include('inc/informacoes/informacoes-sub-menu-categoria.php'); ?> </ul>
                </nav> <br>
            </div>
        </aside>