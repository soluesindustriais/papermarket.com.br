<form class="search search--topo d-none-mobile" action="<?=$url?>pesquisa" method="post">
    <input type="text" placeholder="O que você procura no <?=$nomeSite?> ?" name="palavra" id="Buscar">
    <button type="submit" aria-label="Buscar"><i class="fas fa-search"></i></button>
</form>
