<?php if (!$isMobile) : ?>
    <header id="scrollheader">
        <div class="wrapper">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <div class="logo">
                        <a rel="nofollow" href="<?= $url ?>" title="Voltar a página inicial"><img src="<?= $url ?>imagens/logo-solucs.png" alt="<?= $slogan . " - " . $nomeSite ?>" title="<?= $slogan . " - " . $nomeSite ?>"></a>
                    </div>
                    <nav id="menu2">
                        <ul>
                            <? include('inc/menu-top.php'); ?>
                        </ul>
                    </nav>
                </div>
                <? include('inc/pesquisa-inc.php'); ?>
            </div>
        </div>
        
    </header>
    <div id="header-block"></div>
<?php else : ?>
    <header class="header-mobile">
        <div class="wrapper">
            <div class="header-mobile__logo">
                <a rel="nofollow" href="<?= $url ?>" title="Voltar a página inicial">
                    <img src="<?= $url ?>imagens/logo-site.png" alt="<?= $nomeSite ?>" title="<?= $nomeSite ?>" width="399" height="88">
                </a>
            </div>
            <div class="header__navigation">
                <!--navbar-->
                <nav id="menu-hamburger">
                    <!-- Collapse button -->
                    <div class="menu__collapse">
                        <button class="collapse__icon" aria-label="Menu">
                            <span class="collapse__icon--1"></span>
                            <span class="collapse__icon--2"></span>
                            <span class="collapse__icon--3"></span>
                        </button>
                    </div>
                    <!-- collapsible content -->
                    <div class="menu__collapsible">
                        <div class="wrapper">
                            <!-- links -->
                            <ul class="menu__items droppable">
                                <? include('inc/menu-top-hamburger.php'); ?>
                            </ul>
                            <!-- links -->
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- collapsible content -->
                </nav>
                <!--/navbar-->
            </div>
        </div>
    </header>
<?php endif; ?>