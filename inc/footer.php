<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?= $nomeSite . " - " . $slogan ?></span>
			</address>
			<br>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<?php

					foreach ($menu as $key => $value) {
						echo '
								<li>
									<a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : $url . $value[0]) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
								</li>
								';
					}

					?>
					<li><a href="<?= $url ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper">
		Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
		<div class="selos">
			<a rel="nofollow" href="https://validator.w3.org/nu/?showsource=yes&doc=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fa-brands fa-html5"></i><strong>W3C</strong></a>
			<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fa-brands fa-css3"></i><strong>W3C</strong></a>
		</div>
	</div>
</div>
<?php
include "inc/header-fade.php";
include "inc/readmore.php";
include "inc/header-scroll.php";
?>
<script>
    setTimeout(function() {
        var script = document.createElement('script');
        script.src = "https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js";
        document.head.appendChild(script);
    }, 3000);
</script>

<!-- se não tiver chat no satélite, remova ou comente esse script -->
<script>
    setTimeout(function() {
        var script = document.createElement('script');
        script.src = "//code.jivosite.com/widget/";
        document.head.appendChild(script);
    }, 5000);
</script>


<?php if (isset($breadJsonEncoded)) :
    echo "<script type='application/ld+json'>" . $breadJsonEncoded . "</script>";
endif; ?>

<?php
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
include "inc/classes/criacaodepagina.php";
}
?>

<!-- ANALYTICS -->
<?php
foreach($tagsanalytic as $analytics){
	echo '<script async src="https://www.googletagmanager.com/gtag/js?id='.$analytics.'"></script>'.'<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}'."gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?>
<!-- fim ANALYTICS -->
<script src="//code.jivosite.com/widget/HDYoUv2u2a"></script>

        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>