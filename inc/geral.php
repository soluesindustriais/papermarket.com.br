<?
// $analytics = 'id da tag';
$tagsanalytic = ["G-Z2ZK95ESR0","G-D23WW3S4NC"]; // não apagar a segunda tag. Altere somente a primeira.
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
    $url = $http . "://" . $host . "/";
} else {
    $url = $http . "://" . $host . $dir["dirname"] . "/";
}
$nomeSite      = 'PAPERMARKET';
$slogan        = 'Conforto e Praticidade em Cada Folha!';
// $url				= 'http://localhost/site_base_full_beta/';
// $url				= 'http://mpitemporario.com.br/projetos/site_base_full_beta/';
$rua        = 'Rua Alexandre Dumas';
$bairro        = 'Santo Amaro';
$cidade        = 'São Paulo';
$UF          = 'SP';
$cep        = 'CEP: 04717-004';
$latitude      = '-22.546052';
$longitude      = '-48.635514';
$mapa        = 'IFRAME_MAPA';
$senhaEmailEnvia  = '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode      = explode("/", $_SERVER['REQUEST_URI']);
$urlPagina       = end($explode);
$urlPagina       = str_replace('.php', '', $urlPagina);
$urlPagina       == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//********************COM SIG APAGAR********************
// REDES SOCIAIS
$urlFacebook      = 'https://www.facebook.com/plataformasolucoesindustriais';
$urlInstagram        = 'https://www.instagram.com/solucoesindustriaisoficial/';
$urlYouTube          = 'https://www.youtube.com/@solucoesindustriaisbr';
$urlLinkedin         = 'https://br.linkedin.com/company/solucoesindustriais';
$urlTwitter         = 'https://twitter.com/solucoesindustr';
$urlTikTok           = 'URL_COMPLETA_TIKTOK';
//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess, '/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');
$sigMenuPosition = false; 
$sigMenuIcons = [];
//Pasta de imagens, Galeria, url Facebook, etc.
$pasta         = 'imagens/informacoes/';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco

$menu[0] = array('', 'Inicio', false, NULL);
$menu[1] = array('sobre-nos', 'Sobre nós', false, NULL);
$menu[2] = array('produtos-categoria', 'Produtos', 'sub-menu', 'fa-solid fa-chevron-down');



include('inc/vetCategorias.php');
include('inc/classes/trataString.class.php');
include('inc/classes/criarCategoria.class.php');
$trata = new Trata();
$categorias = new Categoria();
$categorias->setCategorias($vetCategorias);

//Função para gerar os arquivos *-categoria.php de cada categoria na raiz e gerar os folders de cada categoria na inc/
//Comentar a mesma ao fazer o deploy
// $categorias->criarCategoria();
// MENU
//Instruções de uso do menu JSON no arquivo json-menu.txt
$jsonMenu = file_get_contents("js/menu-items.json");
$menuItems = json_decode($jsonMenu, true);
$menuKeys = array_keys($menuItems);

//Breadcrumbs
// BREADCRUMB

$breadPosition = 1;
$breadJsonSchema = array(
  "@context" => "https://schema.org",
  "@type" => "BreadcrumbList",
  "itemListElement" => array(
    [
      "@type" => "ListItem",
      "position" => $breadPosition,
      "item" => array(
        "@id" => trim($url, '/'),
        "name" => "Home"
      )
    ]
  )
);

if (!function_exists('addBreadJson')) {
  function addBreadJson($urlBread, $title)
  {
    global $breadPosition, $breadJsonSchema, $url;
    $breadPosition++;
    array_push($breadJsonSchema["itemListElement"], ["@type" => "ListItem", "position" => $breadPosition, "item" => array("@id" => $url . $urlBread, "name" => $title)]);
  }
}

$caminho = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row wrapper-produtos">
<h1>' . $h1 . '</h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="2">
            </li>
        </ol>
    </nav>
    
</div>    
</div>';
$caminhocateg = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row wrapper-produtos">
<h1>' . $h1 . '</h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'produtos-categoria" itemprop="item" title="Produtos">
                <span itemprop="name"> Produtos</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="3">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
