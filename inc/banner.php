<main>
	<?php if (!$isMobile) : ?>
		<!--STARTBANNER-->
		<div class="slick-banner">
			<!-- HERO WITH THUMBNAIL IMAGE EXAMPLE -->
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-2.jpg');">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h2>Procura papel higiênico?</h2>
							<p>Fabricação de papel higienico de diversas metragens<br> faça uma cotação!</p>
							<a class="btn" href="<?= $url ?>distribuidor-de-papel-higienico" title="">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/logo-site.png" alt="" title="" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<!-- HERO WITH TEXT ONLY EXAMPLE -->
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-1.jpg')">
				<div class="content-banner">
					<h2>Papel toalha!</h2>
					<p>Ganhe agilidade e economia com a utilização de papel toalha, venha conferir nossos preços.</p>
					<a class="btn" href="<?= $url ?>pacote-de-papel-toalha" title="">Clique</a>
				</div>
			</div>
			<!-- NIVO EXAMPLE -->
			<!-- <a href="<?= $url ?>duda-godoi" title="">
					<img data-lazy="<?= $url ?>imagens/banner/banner-exemplo.webp" src="#" alt="" title="">
				</a> -->
		</div>
		<!--ENDBANNER-->
	<?php endif; ?>