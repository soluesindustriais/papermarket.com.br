<?php
$h1         = 'Início';
$title      = 'Início';
$desc       = 'Início';
$key        = '';
$var        = 'Início';
include "inc/head.php";
?>
<style>
	<?php
	include "slick/slick.css";
	if (!$isMobile) :
		include "slick/slick-banner.css";
	endif;
	?>
</style>

</head>

<body>
	<?php
	include "inc/topo.php";
	include "inc/banner.php";
	include "inc/block-1.php";
	include "inc/block-3.php";
	include "inc/footer.php";
	?>
	</main>
	<script>
		<?php include "slick/slick.min.js"; ?>
	</script>
	<script>
		$(document).ready(function(){
			<?php if(!$isMobile): ?>
				$('.slick-banner').slick({
					fade: false,
					cssEase: 'ease',
					autoplay: true,
					infinite: true,
					speed: 500,
					dots: true,
					lazyLoad: 'ondemand',										
					swipeToSlide: true,		
				});
			<?php endif; ?>

			$('.produtos__carousel').slick({
				autoplaySpeed: 3000,
				autoplay: true,
				speed: 500,
				infinite: true,
				cssEase: 'ease',
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: true,				
				responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						dots: false,
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						dots: false,
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						dots: false,
					}
				}
				]
			});
		});

	</script>
	<script defer src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>

</html>