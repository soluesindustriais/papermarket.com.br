<? $h1 = "Produtos";
$title  = "Produtos";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
?>
<body> <? include("inc/topo.php"); ?>
    <main>
        <div class="content">
            <section> <?= $caminho ?>
                <div class="wrapper-produtos">
                    <article class="full">
                        <h2>Conheça nossos Produtos</h2>
                        <ul class="thumbnails-main">
                            <?php
                            foreach ($vetCategorias as $categoria) {
                                $categoriaNameSemAcento = strtolower(remove_acentos($categoria)); // Remove acentos e substitui espaços por hifens
                                $CategoriaNameUpper = ucwords(str_replace('-', ' ', $categoria)); // Substitui hifens por espaços e coloca a primeira letra de cada palavra em maiúscula

                                // Gerar um número aleatório entre 1 e 12
                                $numeroAleatorio = rand(1, 4);

                                // Estrutura HTML do elemento LI
                                echo "<li>
                                        <a rel=\"nofollow\" href=\" $url$categoriaNameSemAcento-categoria\" title=\"$CategoriaNameUpper\">
                                            <div class=\"overflow-hidden\">
                                                <img src=\"$url" . "imagens/$categoriaNameSemAcento/$categoriaNameSemAcento-$numeroAleatorio.webp\" alt=\"$CategoriaNameUpper\" title=\"$CategoriaNameUpper\">

                                            </div>
                                            <div class=\"title-produtos\">
                                                <h2>$CategoriaNameUpper</h2>
                                            </div>
                                        </a>
                                    </li>";
                            } ?>
                        </ul>
                    </article>
                </div>
            </section>
        </div>
    </main>
    <!-- .wrapper --> <? include("inc/footer.php"); ?>
</body>

</html>