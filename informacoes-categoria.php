<? $h1 = "Informações - Categoria";
$title  = "Informações - Categoria";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
include("inc/informacoes/informacoes-vetPalavras.php"); ?>

<body> <? include("inc/topo.php"); ?>
    <main>
        <div class="content">
            <section> <? include'inc/auto-breadcrumb.php' ?>
                <div class="wrapper-produtos">
                    <?php include_once("inc/informacoes/informacoes-buscas-relacionadas.php"); ?>
                    <br class="clear">
                    <h1 style="text-align: center;"><?= $h1 ?></h1>
                    <article class="full">
                        <div class="article-content">
                            <p>O mercado de <?= $h1 ?> é amplo e conta com produtos e serviços que podem ser úteis em diversas aplicações. No Soluções Industriais, portal especializado na geração de negócios para o mercado B2B, é possível encontrar as melhores empresas que atuam nesse segmento.</p>
                        </div>
                        <ul class="thumbnails-main"> <?php include_once("inc/informacoes/informacoes-categoria.php"); ?> </ul>
                    </article>
                </div>
            </section>
        </div>
    </main>
    <!-- .wrapper --> <? include("inc/footer.php"); ?>
</body>

</html>