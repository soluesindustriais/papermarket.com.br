<?php
 $h1 = "Artigos";
$title  = "Artigos";
$desc = "";
$key  = "";
include "inc/head.php"; ?>

<link rel="stylesheet" href="<?php echo $url ?>css/thumbnails.css">
</head>

<body>
    <?php include "inc/topo.php"; ?>
    <main role="main">
        <div class="content">
            <section> <?php echo $caminho ?>
                <div class="wrapper-produtos">
                    <?php include_once "inc/artigos/artigos-buscas-relacionadas.php"; ?>
                    <br class="clear" />
                    <article class="full">
                        <ul class="thumbnails-main"> <?php include_once "inc/artigos/artigos-categoria.php" ?> </ul>
                    </article> <br class="clear">
            </section>
        </div>
    </main>
    </div><!-- .wrapper --> <?php include "inc/footer.php"; ?>
</body>
</html>