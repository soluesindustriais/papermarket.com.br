<?
$h1         = 'Sobre nós';
$title      = 'Sobre nós';
$desc       = 'sobre nos';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Sobre nós';
include('inc/head.php');
?>

<style>

    main.about-us h1 {
        margin-bottom: -200px;
        font-weight: 400;
    }

    main.about-us h1 {
        margin-bottom: -65px;
        text-align: center;
        font-size: 4.5em;
    }

    main.about-us h2 {
        color: #fff;
        letter-spacing: 5px;
        font-weight: 300;
    }

    .content-about-us {
        padding: 20px;
        margin-top: -5px;
        color: white;
        background-color: #071A2B;
    }

    .content-about-us h2,
    .us-feedbacks h2 {
        font-size: 3em;
    }

    .content-about-us p {
        font-family: var(--font-primary);
        padding-top: 15px;
        font-size: 1.1em;
        margin-top: 0px;
    }

    .circle-green {
        padding: 10px;
        background-color: green;
        border-radius: 50%;
        width: 40px;
        text-align: center;
        height: 40px;
        margin-bottom: 10px;
    }

    .our-culture-content {
        width: 41%;
    }

    .our-culture-container {
        margin-top: 45px;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }

    .our-culture-container h3 {
        height: 30px;
        text-transform: uppercase;
    }

    .content-about-us h3 {
        letter-spacing: 1px;
        color: #fff;
        font-size: 1.2em;
    }


    @media only screen and (min-width:1366px) {
        main.about-us h1 {
            margin-bottom: -200px;
        }

        .our-culture-container h3 {
            height: auto;
            font-size: 2em;
        }

        .our-culture-content {
            width: 44%;
        }

        .content-about-us p {
            font-size: 1.5em;
        }

        .circle-green {
            display: flex;
            font-size: 2em;
            border-radius: 50%;
            width: 100px;
            height: 100px;
            align-items: center;
            justify-content: center;
        }
    }

    .content-about-us a {
        color: #57a3ff;
    }

    .img-about-us {
        display: flex;
        justify-content: center;
    }

    .us-feedbacks {
        padding: 20px;
    }

    .us-feedbacks p {
        font-size: 1.5em;
    }

    .feedbacks img {
        width: 160px;
    }

    .container-feedbacks {
        display: flex;
        justify-content: center;
    }

    .feedbacks {
        display: flex;
        margin: 20px;
        width: 25%;
        flex-direction: column;
        border-radius: 5px;
        align-items: center;
        background-color: rgba(0, 0, 0, 0.15);
    }

    .feedbacks h3 {
        font-size: 1em;
        text-align: center;
        color: #0079b0;
    }

    .feedbacks p {
        text-align: center;
    }



    .team-content {
        display: flex;
    }

    .team-info {
        width: 60%;
        margin-right: 15px;
    }

    .team-info h2 {
        font-size: 6em;
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .team-info p {
        font-size: 1.3em;
    }

    .about-team {
        padding: 20px;
        background-color: #cccccc73;
    }

    @media only screen and (max-width:765px) {
        .feedbacks {
            width: auto;
        }

        .container-feedbacks {
            flex-direction: column;
        }

        .team-info {
            width: 100%;
        }

        .team-content {
            flex-direction: column;
        }
    }
</style>

</head>


<body>
    <? include('inc/topo.php'); ?>
    <?= $caminho ?>
    <main class="about-us">

        <h1 class="font-montserrat">Sobre <br> Nós</h1>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#071A2B" fill-opacity="1" d="M0,288L34.3,282.7C68.6,277,137,267,206,266.7C274.3,267,343,277,411,282.7C480,288,549,288,617,282.7C685.7,277,754,267,823,250.7C891.4,235,960,213,1029,170.7C1097.1,128,1166,64,1234,37.3C1302.9,11,1371,21,1406,26.7L1440,32L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z"></path>
        </svg>
        <div class="content-about-us">
            <div class="wrapper-produtos">
                <p>O Soluções Industriais é uma empresa do Grupo Ideal Trends (GIT), criada em 2010 para atender às necessidades das indústrias. O GIT é composto por mais de 30 empresas em diversos setores. </p>
                <p>A plataforma do Soluções Industriais conecta compradores e fornecedores, facilitando cotações. Suas estratégias de Marketing visam aumentar a visibilidade online dos clientes. Isso é alcançado por meio da captação de tráfego qualificado em mais de 100 canais.</p>
                <p>O Soluções Industriais é uma solução inovadora para empresários que desejam crescer com o Marketing Digital.</p>
                <div class="img-about-us"><img src="<?= $url ?>imagens/sobrenos/about-us.png" alt=""></div>

                <h2 class="font-montserrat">Nossa Cultura</h2>
                <div class="our-culture-container">
                    <div class="our-culture-content ">
                        <div class="circle-green">05.</div>
                        <h3 class="font-montserrat">Resultados</h3>
                        <p>Resultados são a força motriz da empresa. O foco nos resultados nos permite concentrar tempo e energia no que é essencial.</p>
                    </div>
                    <div class="our-culture-content font-montserrat">
                        <div class="circle-green">06.</div>
                        <h3 class="font-montserrat">Agir como dono</h3>
                        <p>Somos todos donos da empresa. E um dono assume a responsabilidade pelos resultados pessoalmente.</p>
                    </div>
                    <div class="our-culture-content ">
                        <div class="circle-green">09.</div>
                        <h3 class="font-montserrat">Trabalho Duro</h3>
                        <p>Nunca estamos plenamente satisfeitos com nossos resultados. Pois evita acomodação à situação atual.</p>
                    </div>
                    <div class="our-culture-content ">
                        <div class="circle-green">10.</div>
                        <h3 class="font-montserrat">Integridade</h3>
                        <p>Não tomamos atalhos. Integridade, trabalho duro e consistência são o cimento que pavimenta nossa empresa.</p>
                    </div>
                </div>
                <p>Princípios essenciais estão mencionados aqui; explore os 10 <a target="_blank" href="https://idealtrends.com.br/principios/">em nossa página dedicada</a> para saber mais.</p>
            </div>
        </div>

        <div class="us-feedbacks">
            <div class="wrapper-produtos">
                <h2 style="color: #012f73;">NOSSOS FEEDBACKS</h2>
                <p>Em mais de 13 anos de empresa, o Soluções Industriais conquistou a confiança de milhares de clientes. Confira o que alguns dos nossos anunciantes estão falando:</p>
                <div class="container-feedbacks">
                    <div class="feedbacks">
                        <img src="<?= $url ?>imagens/sobrenos/junseal.png" alt="">
                        <h3>SÉRGIO THOMAZELLI</h3>
                        <p>"Nós estamos sentido uma diferença no número de vendas."</p>
                    </div>
                    <div class="feedbacks">
                        <img src="<?= $url ?>imagens/sobrenos/maze.png" alt="">
                        <h3>MAURO SANCHEZ</h3>
                        <p>"Temos retorno com o Soluções Industriais, novos contatos comerciais e novas prospecções."</p>
                    </div>
                    <div class="feedbacks">
                        <img src="<?= $url ?>imagens/sobrenos/itaqua.png" alt="">
                        <h3>MAURO SANCHEZ</h3>
                        <p>"Temos retorno com o Soluções Industriais, novos contatos comerciais e novas prospecções."</p>
                    </div>
                    <div class="feedbacks">
                        <img src="<?= $url ?>imagens/sobrenos/tec-em-calor.png" alt="">
                        <h3>MAURO SANCHEZ</h3>
                        <p>"Temos retorno com o Soluções Industriais, novos contatos comerciais e novas prospecções."</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-team">
            <div class="wrapper-produtos">
                <div class="team-content">
                    <div class="team-info">
                        <h2 style="color: #012f73;">CONHEÇA NOSSO TIME</h2>
                        <p>O Soluções Industriais conta com uma equipe de excelência, desde as áreas de Vendas, Novos Negócios e Customer Success, atreladas ao atendimento dos clientes, até os setores Operacionais em que os especialistas de Marketing Digital cuidam dos projetos da casa.</p>
                        <p>Todos do nosso time realizam rigorosos controles de qualidade com o objetivo de aprimorar cada vez mais os processos internos e entregar as conquistas desejadas por empresas de diversos segmentos.</p>
                        <p>O sucesso é uma escolha, e o Soluções Industriais é a melhor delas para o futuro do seu negócio.</p>
                    </div>
                    <div class="team-img">
                        <img src="<?= $url ?>imagens/sobrenos/about-us-2.png" alt="">
                    </div>
                </div>
            </div>
        </div>

    </main>
    <? include('inc/footer.php'); ?>
</body>


</html>